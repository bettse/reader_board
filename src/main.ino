/***************************************************

  This is our GFX example for the Adafruit ILI9341 Breakout and Shield
  ----> http://www.adafruit.com/products/1651

  Check out the links above for our tutorials and wiring diagrams
  These displays use SPI to communicate, 4 or 5 pins are required to
  interface (RST is optional)
  Adafruit invests time and resources providing this open source code,
  please support Adafruit and open-source hardware by purchasing
  products from Adafruit!

  Written by Limor Fried/Ladyada for Adafruit Industries.
  MIT license, all text above must be included in any redistribution
 ****************************************************/

#include "ILI9341_t3.h"
#include "SPI.h"
#include "font_Arial.h"
#include <Wiegand.h>
#include <font_AwesomeF000.h>
#include <font_LiberationMono.h>

#define N sizeof valid / sizeof valid[0]

#define PINCHANGE(idx, pin_0, pin_1)               \
  void pinStateChanged##idx() {                    \
    readers[idx].setPin0State(digitalRead(pin_0)); \
    readers[idx].setPin1State(digitalRead(pin_1)); \
  }

#define PINSETUP(idx, pin_0, pin_1)                                            \
  pinMode(pin_0, INPUT_PULLDOWN);                                              \
  pinMode(pin_1, INPUT_PULLDOWN);                                              \
  readers[idx].onReceive(receivedData, readerIds + idx);                       \
  readers[idx].onReceiveError(receivedDataError, readerIds + idx);             \
  readers[idx].onStateChange(stateChanged, readerIds + idx);                   \
  readers[idx].begin(Wiegand::LENGTH_ANY, false);                              \
  attachInterrupt(digitalPinToInterrupt(pin_0), pinStateChanged##idx, CHANGE); \
  attachInterrupt(digitalPinToInterrupt(pin_1), pinStateChanged##idx, CHANGE); \
  pinStateChanged##idx();

#define TFT_MISO 12
#define TFT_SCLK 13
#define TFT_MOSI 11
#define TFT_DC 9
#define TFT_RST 255
// Actually connected straight to ground
#define TFT_CS 10

ILI9341_t3 tft = ILI9341_t3(TFT_CS, TFT_DC, TFT_RST, TFT_MOSI, TFT_SCLK, TFT_MISO);

#define DEFAULT_FONT Arial_40

#define READER_COUNT 10
uint8_t readerIds[READER_COUNT] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
bool readerState[READER_COUNT] = {false};
Wiegand readers[READER_COUNT] = {
    Wiegand(),
    Wiegand(),
    Wiegand(),
    Wiegand(),
    Wiegand(),
    Wiegand(),
    Wiegand(),
    Wiegand(),
    Wiegand(),
    Wiegand(),
};

PINCHANGE(0, 0, 1)
PINCHANGE(1, 2, 3)
PINCHANGE(2, 4, 5)
PINCHANGE(3, 6, 7)
PINCHANGE(4, 24, 25)
PINCHANGE(5, 26, 27)
PINCHANGE(6, 28, 29)
PINCHANGE(7, 30, 31)
PINCHANGE(8, 34, 35)
PINCHANGE(9, 36, 37)

struct credential {
  uint8_t fc;
  uint16_t cn;
};

struct credential valid[] = {
    {0, 1337},
    {3, 1337}
};

// Notifies when a reader has been connected or disconnected.
// Instead of a message, the seconds parameter can be anything you want -- Whatever you specify on `wiegand.onStateChange()`
void stateChanged(bool plugged, uint8_t *readerNumber) {
  Serial.print(*readerNumber);
  Serial.print(" ");
  Serial.println(plugged ? "CONNECTED" : "DISCONNECTED");
  readerState[*readerNumber] = plugged;
  showReaderState();
}

// Notifies when a card was read.
// Instead of a message, the third parameter can be anything you want -- Whatever you specify on `wiegand.onReceive()`
void receivedData(uint8_t *data, uint8_t bits, uint8_t *readerNumber) {
  tft.fillRect(0, 0, tft.width(), tft.height() / 2, ILI9341_BLACK);
  tft.setCursor(0, 0);

  Serial.print("Max bytes = ");
  Serial.print(Wiegand::MAX_BYTES);
  Serial.println("");

  Serial.print("(");
  Serial.print(bits);
  Serial.print(") ");

  tft.setFont(LiberationMono_40);
  tft.print(bits);
  tft.println(" bit");

  uint8_t count = (bits + 7) / 8;
  for (int i = 0; i < count; i++) {
    Serial.print(data[i] >> 4, HEX);
    Serial.print(data[i] & 0xF, HEX);

    tft.print(data[i] >> 4, HEX);
    tft.print(data[i] & 0xF, HEX);
  }
  Serial.println("");
  tft.println("");
  tft.setFont(DEFAULT_FONT);
}

// Notifies when an invalid transmission is detected
void receivedDataError(Wiegand::DataError error, uint8_t *rawData, uint8_t rawBits, uint8_t *readerNumber) {
  Serial.print(Wiegand::DataErrorStr(error));
  Serial.print(" - Raw data: ");
  Serial.print(rawBits);
  Serial.print("bits / ");

  //Print value in HEX
  uint8_t bytes = (rawBits + 7) / 8;
  for (int i = 0; i < bytes; i++) {
    Serial.print(rawData[i] >> 4, HEX);
    Serial.print(rawData[i] & 0xF, HEX);
  }
  Serial.println();
}

void error(const __FlashStringHelper *err) {
  Serial.println(err);
  //while (1);
}

void showReaderState() {
  uint16_t h, w;
  tft.setFont(AwesomeF000_18);
  tft.setTextColor(ILI9341_YELLOW);
  tft.measureChar((char)5, &w, &h);
  // Adjustments for spacing
  w += 10;
  h += 10;

  for (size_t i = 0; i < READER_COUNT; i++) {
    tft.setCursor(i * w, tft.height() - h);
    tft.fillRect(i * w, tft.height() - h, w, h, ILI9341_BLACK);
    bool state = readerState[i];
    if (state) {
      tft.print((char)5);
    } else {
      tft.print((char)6);
    }
    Serial.printf("Reader (%d, Y%d): %s\n", i * w, tft.height() - h, state ? "On" : "Off");
  }

  tft.setFont(DEFAULT_FONT);
}

void printAwesome(char i, uint8_t x, uint8_t y) {
  tft.setFont(AwesomeF000_18);
  tft.setCursor(x, y);
  tft.print(i);
  tft.setFont(DEFAULT_FONT);
}

void access_granted() {
  const size_t speed = 1500;
  tft.setFont(LiberationMono_40);
  const uint16_t width = tft.measureTextWidth("ACCESS", 0);
  const uint16_t height = tft.measureTextHeight("ACCESS", 0);

  tft.fillScreen(ILI9341_BLACK);
  tft.setTextColor(ILI9341_GREEN);
  tft.setCursor((tft.width() - width) / 2, tft.height() / 3 - height / 2);
  tft.println("ACCESS");
  tft.setCursor((tft.width() - width) / 2, tft.height() * 2 / 3 - height / 2);
  tft.println("GRANTED");

  delay(speed);

  tft.fillScreen(ILI9341_BLACK);
  tft.setTextColor(ILI9341_YELLOW);

  showReaderState();
}

void access_denied() {
  const size_t repeats = 3;
  const size_t speed = 500;
  tft.setFont(LiberationMono_40);
  const uint16_t width = tft.measureTextWidth("ACCESS", 0);
  const uint16_t height = tft.measureTextHeight("ACCESS", 0);

  for (size_t i = 0; i < repeats; i++) {
    tft.fillScreen(ILI9341_BLACK);
    tft.setTextColor(ILI9341_RED);
    tft.setCursor((tft.width() - width) / 2, tft.height() / 3 - height / 2);
    tft.println("ACCESS");
    tft.setCursor((tft.width() - width) / 2, tft.height() * 2 / 3 - height / 2);
    tft.println("DENIED");
    delay(speed);

    // Invert
    tft.fillScreen(ILI9341_RED);
    tft.setTextColor(ILI9341_BLACK);
    tft.setCursor((tft.width() - width) / 2, tft.height() / 3 - height / 2);
    tft.println("ACCESS");
    tft.setCursor((tft.width() - width) / 2, tft.height() * 2 / 3 - height / 2);
    tft.println("DENIED");
    delay(speed);
  }
  tft.fillScreen(ILI9341_BLACK);
  tft.setTextColor(ILI9341_YELLOW);

  showReaderState();
}

void setup() {
  tft.begin();
  // Note: you can now set the SPI speed to any value
  // the default value is 30Mhz, but most ILI9341 displays
  // can handle at least 60Mhz and as much as 100Mhz
  tft.setClock(10000000);
  tft.fillScreen(ILI9341_BLACK);
  tft.setRotation(3);
  tft.setTextColor(ILI9341_YELLOW);
  tft.setFont(DEFAULT_FONT);
  //tft.setTextSize(2);
  tft.println("Ready");

  Serial.begin(115200);
  // while (!Serial) ; // wait for Arduino Serial Monitor

  showReaderState();

  PINSETUP(0, 0, 1)
  PINSETUP(1, 2, 3)
  PINSETUP(2, 4, 5)
  PINSETUP(3, 6, 7)
  PINSETUP(4, 24, 25)
  PINSETUP(5, 26, 27)
  PINSETUP(6, 28, 29)
  PINSETUP(7, 30, 31)
  PINSETUP(8, 34, 35)
  PINSETUP(9, 36, 37)
}

void loop(void) {
  noInterrupts();
  for (size_t i = 0; i < READER_COUNT; i++) {
    readers[i].flush();
  }
  interrupts();
  delay(100);
}
