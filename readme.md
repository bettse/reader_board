# Reader Board

Software for a teensy + [display](https://www.pjrc.com/store/display_ili9341_touch.html) for monitoring the state of my collection of readers and show the credentials read.

<img width="200" alt="Reader Board" src="./demo.jpg" style="display: block; margin: 0 auto;"/>


### Idead
 * Add support for touchscreen
 * Add sd card to teensy to store credentials
 * Wire readers light, tamper, and beep to teensy
 * Define credentials that show "access granted" or "access denied" on screen

